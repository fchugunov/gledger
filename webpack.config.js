const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    mode: 'production',

    optimization: {
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    compress: {
                        properties: false
                    },
                    output: {
                        keep_quoted_props: true,
                        quote_keys: true,
                        // beautify: true
                    }
                },
            }),
        ],
    },

    entry:{
        lib:'./src/lib.ts'
    },

    output: 
    {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        libraryTarget: 'var',
        library: 'gledger'
    },

    module:
    {
        rules: [
            {
                test: /\.ts$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                [
                                    '@babel/preset-env',                  
                                    {
                                        // modules: 'false'
                                        useBuiltIns: 'usage',
                                        corejs: 3,                                        
                                        modules: 'commonjs'
                                    },
                                ]
                            ],
                        }
                    },
                    
                    {
                        loader: 'ts-loader',
                        // options: {
                        //     configFile: 'tsconfig.webpack.json'
                        // }
                    }
                ]
            }
        ]
    },

    resolve: {
        extensions: ['.js', '.ts']
    },

    plugins: [
        new CopyPlugin([
            'src/api.js',
            'appsscript.json',
            '.clasp.json'
        ])
    ]
};
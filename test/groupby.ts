import { expect } from 'chai';
import { describe, it } from 'mocha';

import { groupby } from '../src/groupby';

describe('#groupby()', () => {

    var params = {
        range: null,
        groupby: null,
        sum: null,
        result: null
    }; 

    var sortFunction = function(a, b) {
        if (a[0] === b[0]) {
            return 0;
        }
        else {
            return (a[0] < b[0]) ? -1 : 1;
        }
    }

    var sortResult = function(a:any[][]) {
        return [a[0]].concat(a.slice(1).sort(sortFunction));
    }
    
    describe('input paramers type check', () => {

        beforeEach(() => {
            params.range = [
                ['a', 'b', 'c'],
                [1, 2, 3],
                [1, 2, 3]
            ];

            params.groupby = [
                ['a', 'b']
            ];

            params.sum = [
                ['c']
            ];

        });

        describe('range param', () => {          

            it('first row must be array', () => {
                params.range[0] = 'asd';
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'range');
            }),

            it('first row values cannot be "null"', () => {                
                params.range[0][1] = null;
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'range');
            }),

            it('first row values cannot be empty', () => {
                params.range[0][1] = '';
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'range');
            }),

            it('must have all columns specified in groupby parameter', () => {
                params.range[0][0] = 'unknown';
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'range');
            })

            it('must have all columns specified in sum parameter', () => {
                params.range[0][2] = 'unknown';
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'range');
            })

        });

        describe('groupby param', () => {

            it('parameter may have only one row', () => {
                params.groupby = [['a'], ['b']];
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'groupby');
            }),

            it('any value in parameter cannot be "null"', () => {
                params.groupby[1] = null;
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'groupby');
            }),

            it('any value in parameter cannot be empty', () => {
                params.groupby[1] = '';
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'groupby');
            }),

            it('may consist of one column string value', () => {
                params.groupby = 'a';
                expect(() => groupby(params.range, params.groupby, params.sum)).to.not.throw(TypeError, 'groupby');
            })
        })


        describe('sum param', () => {

            it('parameter may have only one row', () => {
                params.sum = [['a'], ['b']];
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'sum');
            }),

            it('any value in parameter cannot be "null"', () => {
                params.sum[1] = null;
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'sum');
            }),

            it('any value in parameter cannot be empty', () => {
                params.sum[1] = '';
                expect(() => groupby(params.range, params.groupby, params.sum)).to.throw(TypeError, 'sum');
            }),

            it('may consist of one column string value', () => {
                params.sum = 'a';
                expect(() => groupby(params.range, params.groupby, params.sum)).to.not.throw(TypeError, 'sum');
            })
        })
    });


    describe('result check', () => {

        beforeEach(() => {
            params.range = [
                ['a', 'b', 'c'],
                [1, 2, 1],
                [1, 2, 2],
                [2, 2, 3],
                [2, 2, 4],
                [1, 3, 5],
            ];

            params.groupby = [
                ['a', 'b']
            ];

            params.sum = [
                ['c']
            ];

            params.result = [
                ['a', 'b', 'c'],
                [1, 2, 3],
                [1, 3, 5],
                [2, 2, 7]
            ];

        })

        it('sum over 1 column', () => {
            expect(sortResult(groupby(params.range, params.groupby, params.sum))).to.deep.equal(sortResult(params.result));
        })

        it('sum with null groupby values', () => {
            params.range[1][1] = null;
            params.range[2][1] = null;
            params.result[1][1] = null;
            expect(sortResult(groupby(params.range, params.groupby, params.sum))).to.deep.equal(sortResult(params.result));
        })

        it('sum values may be text int', () => {
            params.range[1][2] = '0.5';
            params.range[2][2] = '2,5';
            expect(sortResult(groupby(params.range, params.groupby, params.sum))).to.deep.equal(sortResult(params.result));
        })
    });
});
import { expect } from 'chai';
import { describe, it } from 'mocha';

import { flatten } from '../src/flatten';

describe('#flatten()', () => {

    var params = {
        columns: null,
        rows: null,
        data: null,
        result: null
    }; 

    beforeEach(() => {

        params.columns = [
            ['c1', 'c2', 'c3', 'c4']
        ];

        params.rows = [
            ['r1'],
            ['r2'],
            ['r3']
        ];

        params.data = [
            ['d11', 'd12', 'd13', 'd14'],
            ['d21', 'd22', 'd23', 'd24'],
            ['d31', 'd32', 'd33', 'd34'],
        ];

        params.result = [
            ['c1', 'r1', 'd11'],
            ['c1', 'r2', 'd21'],
            ['c1', 'r3', 'd31'],            

            ['c2', 'r1', 'd12'],
            ['c2', 'r2', 'd22'],
            ['c2', 'r3', 'd32'],

            ['c3', 'r1', 'd13'],
            ['c3', 'r2', 'd23'],
            ['c3', 'r3', 'd33'],

            ['c4', 'r1', 'd14'],
            ['c4', 'r2', 'd24'],
            ['c4', 'r3', 'd34'],
        ];

    });

    describe('input paramers type check', () => {

        describe('columns param', () => {          

            it('must be one row array', () => {
                params.columns.push(['cc1', 'cc2']);                
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'columns');
            }),

            it('first row values cannot be "null"', () => {                
                params.columns[0][1] = null;
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'columns');
            }),

            it('first row values cannot be empty', () => {
                params.columns[0][1] = '';
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'columns');
            })

        });

        describe('rows param', () => {          

            it('must be one column array', () => {
                params.rows[0].push('rr1');
                params.rows[1].push('rr2');
                params.rows[2].push('rr3');
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'rows');
            }),

            it('first column values cannot be "null"', () => {                
                params.rows[1][0] = null;
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'rows');
            }),

            it('first column values cannot be empty', () => {
                params.rows[0][0] = '';
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'rows');
            })

        });

        describe('data param', () => {          

            it('must be array', () => {
                params.data = 'asd';
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'data');
            }),

            it('columns number same as in columns param', () => {
                params.data[0].pop();
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'data');
            }),

            it('rows number same as in rows param', () => {                
                params.data.pop();
                expect(() => flatten(params.columns, params.rows, params.data)).to.throw(TypeError, 'data');
            })

        });

    });

    describe('result check', () => {

        it('example 1', () => {
            expect(flatten(params.columns, params.rows, params.data)).to.deep.equal(params.result);
        })

    });
});
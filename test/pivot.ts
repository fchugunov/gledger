import { expect } from 'chai';
import { describe, it } from 'mocha';

import { pivot } from '../src/pivot';

describe('#pivot()', () => {

    var params = {
        data: null,
        sumFormula: null,
        rowFilter: null,
        columnFilter: null,
        result: null
    }; 
    
    beforeEach(() => {
        params.data = [
            ['a', 'b', 'c'],
            ['r1', 'c1', 1],
            ['r1', 'c1', 2],
            ['r2', 'c1', 3],
            ['r2', 'c2', 4],
            ['r2', 'c3', 5]
        ];

        params.sumFormula = [
            ['={c}', '=-{c}', ""]
        ];

        params.rowFilter = [
            ["={a}=='r1'"],
            ["={a}=='r2'"],
            [""],
            ["={a}=='r3'"]
        ];

        params.columnFilter = [
            ["={b}=='c1'", "={b}=='c2'", ""]
        ];

        params.result = [
            [3, 0, ""],
            [3, -4, ""],
            ["", "", ""],
            [0, 0, ""]

        ];
    });

    describe('input paramers type check', () => {

        describe('sumFormula', () => {          

            it('first row must be array', () => {
                params.sumFormula[0] = 'asd';
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'sumFormula');
            }),

            it('may have only one row', () => {
                params.sumFormula.push(params.sumFormula[0]);
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'sumFormula');
            }),

            it('first row may have "null" values', () => {                
                params.sumFormula[0][1] = null;
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.not.throw(TypeError);
            }),

            it('first row may have empty string values', () => {
                params.sumFormula[0][1] = '';
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.not.throw(TypeError);
            }),

            it('formula params must be column name', () => {
                params.sumFormula[0][1] = '={param1}';
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'param1');
            })

        });

        describe('columnFilter', () => {          

            it('first row must be array', () => {
                params.columnFilter[0] = 123;
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'columnFilter');
            }),

            it('must have only one row', () => {
                params.columnFilter.push(params.columnFilter[0]);
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'columnFilter');
            }),

            it('first row may have "null" values', () => {                
                params.columnFilter[0][1] = null;
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.not.throw(TypeError);
            }),

            it('first row may have empty string values', () => {
                params.columnFilter[0][1] = '';
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.not.throw(TypeError);
            })

        });

        describe('rowFilter', () => {

            it('every row must be array', () => {
                params.rowFilter[1] = 123;
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'rowFilter');
            }),

            it('every row may have only one value', () => {
                params.rowFilter[0].push(params.rowFilter[0][0]);
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'rowFilter');
            }),

            it('may have "null" values', () => {                
                params.rowFilter[0][0] = null;
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.not.throw(TypeError);
            }),

            it('may have empty string values', () => {
                params.rowFilter[1][0] = '';
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.not.throw(TypeError);
            })

        });

        describe('data', () => {          

            it('first row must be array', () => {
                params.data[0] = 'asd';
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'data');
            }),

            it('first row values cannot be "null"', () => {                
                params.data[0][1] = null;
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'data');
            }),

            it('first row values cannot be empty', () => {
                params.data[0][1] = '';
                expect(() => pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.throw(TypeError, 'data');
            })
        });
    });


    describe('pivot test', () => {

        it('simple 1', () => {            

            expect(pivot(params.data, params.sumFormula, params.rowFilter, params.columnFilter)).to.deep.equal(params.result);
        })
    })

});

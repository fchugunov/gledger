import { expect } from 'chai';
import { describe, it } from 'mocha';

import { filter } from '../src/filter';

describe('#filter()', () => {

    var params = {
        range: null,
        filter: null,
        result: null
    }; 

    beforeEach(() => {
        params.range = [
            ['a', 'b', 'c'],
            [1, true, true],
            [2, true, false]
        ];

        params.filter = [
            ['b', 'c']
        ];

        params.result = [
            ['a', 'b', 'c'],
            [1, true, true],
        ];

    });

    describe('input paramers type check', () => {

        describe('range param', () => {          

            it('first row must be array', () => {
                params.range[0] = 'asd';
                expect(() => filter(params.range, params.filter)).to.throw(TypeError, 'range');
            }),

            it('first row values cannot be "null"', () => {                
                params.range[0][1] = null;
                expect(() => filter(params.range, params.filter)).to.throw(TypeError, 'range');
            }),

            it('first row values cannot be empty', () => {
                params.range[0][1] = '';
                expect(() => filter(params.range, params.filter)).to.throw(TypeError, 'range');
            })

        });

        describe('fitler param', () => {

            it('parameter may have only one row', () => {
                params.filter = [['a'], ['b']];
                expect(() => filter(params.range, params.filter)).to.throw(TypeError, 'filter');
            }),

            it('any value in parameter cannot be "null"', () => {
                params.filter[1] = null;
                expect(() => filter(params.range, params.filter)).to.throw(TypeError, 'filter');
            }),

            it('any value in parameter cannot be empty', () => {
                params.filter[1] = '';
                expect(() => filter(params.range, params.filter)).to.throw(TypeError, 'filter');
            }),

            it('may consist of one column string value', () => {
                params.filter = 'a';
                expect(() => filter(params.range, params.filter)).to.not.throw(TypeError, 'filter');
            })

            it('must have all columns specified in filter parameter', () => {
                params.filter[0][0] = 'unknown';
                expect(() => filter(params.range, params.filter)).to.throw(TypeError, 'filter');
            })
            
            it('formula must have all columns specified in filter parameter', () => {
                params.filter = '={b} and {c1}';
                expect(() => filter(params.range, params.filter)).to.throw(TypeError, 'filter');
            })            
        })
    })

    describe('result check', () => {

        it('2 columns filter', () => {
            expect(filter(params.range, params.filter)).to.deep.equal(params.result);
        }),

        it('formula filter', () => {
            params.filter = '={b} and {c}';
            expect(filter(params.range, params.filter)).to.deep.equal(params.result);
        })

    });
});
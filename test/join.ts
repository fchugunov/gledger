import { expect } from 'chai';
import { describe, it } from 'mocha';

import { join } from '../src/join';

describe('#join()', () => {

    var params = {
        left: null,
        right: null,
        columns: null,
        result: null,
        formula1: null,
        formula2: null
    }; 
    
    describe('input paramers type check', () => {

        beforeEach(() => {
            params.left = [
                ['a', 'b', 'c'],
                [1, 2, 3],
                [1, 2, 3]
            ];

            params.right = [
                ['a', 'b', 'c'],
                [1, 2, 3],
                [1, 2, 3]
            ];

            params.columns = [
                ['a', 'b', 'c']
            ];

        });

        describe('left', () => {          

            it('first row must be array', () => {
                params.left[0] = 'asd';
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'left');
            }),

            it('first row values cannot be "null"', () => {                
                params.left[0][1] = null;
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'left');
            }),

            it('first row values cannot be empty', () => {
                params.left[0][1] = '';
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'left');
            }),

            it('must have all columns specified in columns parameter', () => {
                params.left[0][0] = 'unknown';
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'left');
            })
        });

        describe('right', () => {

            it('first row must be array', () => {
                params.right[0] = 'asd';
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'right');
            }),

            it('first row values cannot be "null"', () => {                
                params.right[0][1] = null;
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'right');
            }),

            it('first row values cannot be empty', () => {
                params.right[0][1] = '';
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'right');
            }),

            it('must have all columns specified in columns parameter', () => {
                params.right[0][0] = 'unknown';
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'right');
            })
        });

        describe('columns', () => {

            it('columns parameter may have only one row', () => {
                params.columns = [['a'], ['b']];
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'columns');
            }),

            it('any value in columns parameter cannot be "null"', () => {
                params.columns[1] = null;
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'columns');
            }),

            it('any value in columns parameter cannot be empty', () => {
                params.columns[1] = '';
                expect(() => join(params.left, params.right, params.columns)).to.throw(TypeError, 'columns');
            }),

            it('may consist of one column string value', () => {
                params.columns = 'a';
                expect(() => join(params.left, params.right, params.columns)).to.not.throw(TypeError, 'columns');
            })

            it('may be empty', () => {
                params.columns = null;
                expect(() => join(params.left, params.right, params.columns)).to.not.throw(TypeError, 'columns');
            })
        })
    });


    describe('join test', () => {

        it('simple 1', () => {

            params.left = [
                ['c1', 'c2', 'l1', 'l2'],
                [1, 1, 'l11', 'l21'],
                [1, 2, 'l12', 'l22'],
                [null, null, null, null],
                [1, 3, 'l13', 'l23'],
                [2, 4, 'l14', 'l24'],
                [null, null, null, null]
            ];

            params.right = [
                ['c1', 'c2', 'r1', 'r2'],
                [1, null, 'r11', 'r21'],
                [null, null, null, null],
                [1, 1, 'r12', 'r22'],
                [null, null, null, null],
                [1, 2, 'r13', 'r23'],
                [null, null, null, null],
                [null, null, null, null]
            ];

            params.columns = null;

            params.result = [
                ['c1', 'c2', 'l1', 'l2', 'r1', 'r2'],
                [1, 1, 'l11', 'l21', 'r12', 'r22'],
                [1, 2, 'l12', 'l22', 'r13', 'r23'],
                [1, 3, 'l13', 'l23', 'r11', 'r21']
            ];

            expect(join(params.left, params.right, params.columns)).to.deep.equal(params.result);
        })
    })


    describe('cross join test', () => {

        it('simple 1', () => {

            params.left = [
                ['l1', 'l2', 'l3'],
                [1, 1, 1],
                [2, 2, 2]
            ];

            params.right = [
                ['r1', 'r2', 'r3'],
                [3, 3, 3],
                [4, 4, 4]
            ];

            params.columns = true;

            params.result = [
                ['l1', 'l2', 'l3', 'r1', 'r2', 'r3'],
                [1, 1, 1, 3, 3, 3],
                [1, 1, 1, 4, 4, 4],
                [2, 2, 2, 3, 3, 3],
                [2, 2, 2, 4, 4, 4]
            ];

            expect(join(params.left, params.right, params.columns)).to.deep.equal(params.result);
        })
    })

    describe('formula test', () => {

        beforeEach(() => {

            params.formula1 = '={l1}+{l2}';
            params.formula2 = '={l1}*{l2}';

            params.left = [
                ['c1', 'c2', 'l1', 'l2'],
                [1, 1, 'l11', 'l21'],
                [1, 2, 'l12', 'l22'],
                [null, null, null, null],
                [1, 3, 'l13', 'l23'],
                [2, 4, 'l14', 'l24'],
                [3, 5, 3, 5],
                [null, null, null, null]
            ];

            params.right = () => [
                ['c1', 'c2', 'r1', 'r2'],
                [1, null, 'r11', 'r21'],
                [null, null, null, null],
                [1, 1, 'r12', 'r22'],
                [null, null, null, null],
                [1, 2, 'r13', 'r23'],
                [3, 5, params.formula1, params.formula2],
                [null, null, null, null],
                [null, null, null, null]
            ];

            params.columns = [
                ['c1', 'c2']
            ];

            params.result = [
                ['c1', 'c2', 'l1', 'l2', 'r1', 'r2'],
                [1, 1, 'l11', 'l21', 'r12', 'r22'],
                [1, 2, 'l12', 'l22', 'r13', 'r23'],
                [1, 3, 'l13', 'l23', 'r11', 'r21'],
                [3, 5, 3, 5, 8, 15]
            ];

        });

        it('simple 1', () => {
            expect(join(params.left, params.right(), params.columns)).to.deep.equal(params.result);
        })

        it('column names may contain whitespaces', () => {
            let c = 'column name with whitespace'
            params.left[0][2] = c
            params.result[0][2] = c
            params.formula1 = '={' + c + '} + {l2}'
            params.formula2 = '={' + c + '} * {l2}'
            expect(join(params.left, params.right(), params.columns)).to.deep.equal(params.result);
        })

        it('unknown column throws error', () => {
            params.formula1 = '={unknown_column} + {l2}'
            expect(() => join(params.left, params.right(), params.columns)).to.throw(TypeError, 'unknown_column');
        })

        it('decimal delimeters may be comma', () => {
            params.formula1 = '=2,05 + 0,95 + {l2}'
            params.formula2 = '=(2,05 + 0,95) * {l2}'
            expect(join(params.left, params.right(), params.columns)).to.deep.equal(params.result);
        })

        it('date function may have dd.mm.yyyy format', () => {
            params.formula1 = '=date("2019-09-01")>date("31.08.2019")'
            params.result[4][4] = true
            expect(join(params.left, params.right(), params.columns)).to.deep.equal(params.result);
        })

        it('format function', () => {
            params.formula1 = '=format(date("2019-09-03"), "01.MM.YYYY")'
            params.formula2 = '=format(date("31.08.2019"), "01.MM.YYYY")'

            params.result[4][4] = '01.09.2019'
            params.result[4][5] = '01.08.2019'

            expect(join(params.left, params.right(), params.columns)).to.deep.equal(params.result);
        })
    })
});

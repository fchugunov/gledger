import { FormulaExpression } from './formula'
import { enableTiming } from './settings'
import { RangeWithColumnNames, ColumnPair, KeyMap, KeyMapArray } from './types'

class JoinFunction {

    protected _leftRange: RangeWithColumnNames;
    protected _rightRange: RangeWithColumnNames;
    protected _columnsArray: string[];
    protected _crossJoin: boolean = false;
    protected _crossJoinColumn: string = '###crossJoin';

    constructor(left: any[][], right: any[][], columns?: any[][] | string | boolean){

        try { this._leftRange = new RangeWithColumnNames(left); }
        catch(e){ throw new TypeError('"left" parameter has invalid value: ' + e.message); }

        try { this._rightRange = new RangeWithColumnNames(right); }
        catch(e){ throw new TypeError('"right" parameter has invalid value: ' + e.message); }

        if (columns == null){
            columns = [this._leftRange.columns.filter(c => this._rightRange.columns.indexOf(c) >= 0)];
        }

        if (typeof columns === "boolean" && columns) {
            this._crossJoin = true;
            this._leftRange.addColumn(this._crossJoinColumn, true);
            this._rightRange.addColumn(this._crossJoinColumn, true);
            columns = this._crossJoinColumn;
        }

        let columnsRange: RangeWithColumnNames;
        try { 
            let columnsTyped:any[][] = typeof columns == 'string' ? [[columns as string]] : columns as any[][];
            columnsRange = new RangeWithColumnNames(columnsTyped);
        }
        catch(e){ throw new TypeError('"columns" parameter has invalid value: ' + e.message); }

        if (columnsRange.length != 0)
            throw new TypeError("'columns' parameter must have only one row");

        this._columnsArray = columnsRange.columns;       

        let notFoundColumn = this._columnsArray.find(c => this._leftRange.columns.indexOf(c) < 0);
        if (notFoundColumn) throw new TypeError(`"left" range does not have "${notFoundColumn}" column`);

        notFoundColumn = this._columnsArray.find(c => this._rightRange.columns.indexOf(c) < 0);
        if (notFoundColumn) throw new TypeError(`"right" range does not have "${notFoundColumn}" column`);
    }

    calc(): any[][]{

        if (enableTiming) console.time('join');
        if (enableTiming) console.time('KeyMapArray');

        let rightKeysMap = new KeyMapArray(this._columnsArray.length);        

        this._rightRange.forEach((v, i) => {
            let keysArray = this._columnsArray.map(c => this._rightRange.get(i, c));

            let lastKey = keysArray.pop();
            
            while(lastKey == null || lastKey == "")
                lastKey = keysArray.pop();

            keysArray = keysArray.concat(lastKey);

            rightKeysMap.add(keysArray.length - 1, keysArray, i);
        })
        
        if (enableTiming) console.timeEnd('KeyMapArray');

        if (enableTiming) console.time('FormulaArray');

        let rightFormulaArray = new Array<Array<FormulaExpression>>();

        for (let i = 0; i < this._rightRange.length; i++) {
            rightFormulaArray.push(new Array(this._leftRange.columns.length));
        }

        this._rightRange.forEach((r, i) => {
            this._rightRange.columns.forEach(c => {

                let rColumnIndex = this._rightRange.columnIndex(c);
                let v:string = this._rightRange.getByIndex(i, rColumnIndex);

                if (typeof v == 'string' && v.startsWith('=')){
                    rightFormulaArray[i - 1][rColumnIndex] = new FormulaExpression(v, this._leftRange.columns);
                }
            })
        })

        if (enableTiming) console.timeEnd('FormulaArray');

        if (enableTiming) console.time('IndexArray');

        let resIndexArray = new Array<Array<number>>();

        this._leftRange.forEach((v, i) => {
            let keysArray = this._columnsArray.map(c => this._leftRange.get(i, c));
            let res = rightKeysMap.get(keysArray);
            if (res != null)
                res.forEach(r => resIndexArray.push([i, r]));        
        })

        if (enableTiming) console.timeEnd('IndexArray');

        if (enableTiming) console.time('headersRow');

        let leftResColumns:Array<ColumnPair> = this._leftRange.columns.filter(
                v => this._columnsArray.indexOf(v) < 0
            ).map(
                c => new ColumnPair(c, this._leftRange.columnIndex(c))
        );

        if (!this._crossJoin){
            leftResColumns =  this._columnsArray.map(c =>
                new ColumnPair(c, this._leftRange.columnIndex(c))
            ).concat(leftResColumns);
        }

        let rightResColumns:Array<ColumnPair> = this._rightRange.columns.filter(
                v => this._columnsArray.indexOf(v) < 0
            ).map(
                c => new ColumnPair(c, this._rightRange.columnIndex(c))
            );

        leftResColumns.forEach((c, i) => {
            if (rightResColumns.some(r => r.name == c.name)) {
                leftResColumns[i].name = c.name + '[1]';
            }
        })

        let headersRow = new Array<string>();
        leftResColumns.forEach((col) => headersRow.push(col.name));
        rightResColumns.forEach((col) => headersRow.push(col.name));
        let resArray = new Array(resIndexArray.length + 1);
        resArray[0] = headersRow;

        if (enableTiming) console.timeEnd('headersRow');

        if (enableTiming) console.time('resArray');

        for (let rowIndex = 0; rowIndex < resIndexArray.length; rowIndex++){            
            let resRow = new Array(leftResColumns.length + rightResColumns.length);

            for (let colIndex = 0; colIndex < leftResColumns.length; colIndex++){
                resRow[colIndex] = this._leftRange._data[resIndexArray[rowIndex][0]][leftResColumns[colIndex].index];
            }

            let leftRowArray = this._leftRange.getRowArray(resIndexArray[rowIndex][0]);
            
            for (let colIndex = 0; colIndex < rightResColumns.length; colIndex++){

                let rightVal = this._rightRange._data[resIndexArray[rowIndex][1]][rightResColumns[colIndex].index];

                let formula = rightFormulaArray[resIndexArray[rowIndex][1] - 1][rightResColumns[colIndex].index];

                if (formula) {
                    rightVal = formula.eval(leftRowArray);
                }

                resRow[leftResColumns.length + colIndex] = rightVal;
            }

            resArray[rowIndex + 1] = resRow;
        }

        if (enableTiming) console.timeEnd('resArray');
        if (enableTiming) console.timeEnd('join');

        return resArray;
    }
}

export function join(left: any[][], right: any[][], columns: any[][] | string): any[][] {
    let f = new JoinFunction(left, right, columns);
    return f.calc();
}
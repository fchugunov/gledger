import { RangeWithColumnNames } from './types'
import { FormulaExpression } from './formula'
import unzip from "pop-zip/unzip";

class FormulaArray {

    protected _expressionArray: FormulaExpression[];    
    protected _columnsArray: string[];

    constructor(formulaRange: any[][], columnsArray: string[], isRow: boolean = true){

        this._columnsArray = columnsArray;

        if(
            !Array.isArray(formulaRange) ||
            (isRow && (formulaRange.length != 1 || !Array.isArray(formulaRange[0]))) ||
            (!isRow && (
                formulaRange.some(x => !Array.isArray(x)) ||
                formulaRange.some(x => x.length != 1)
            ))
        ){
            throw TypeError("Formula range must be single row or single column");
        }

        let formulaArray = (isRow ? formulaRange : unzip(formulaRange))[0];

        this.initExpressionArray(formulaArray);
    }

    protected initExpressionArray(formulaArray: string[]){
        this._expressionArray = new Array(formulaArray.length).fill(null);

        formulaArray.forEach( (v, i) => {
            if (typeof v == 'string' && v.startsWith('=')){
                this._expressionArray[i] = new FormulaExpression(v, this._columnsArray);
            }
        });
    }

    isEmpty(index: number): boolean{
        return this._expressionArray[index] == null;
    }

    eval(index: number, param: any): number{
        let expr = this._expressionArray[index];
        let res = expr.eval(param);
        return res;
    }

    public get length() : number {
        return this._expressionArray.length;
    }
}

class PivotFunction {

    protected _data: RangeWithColumnNames;
    protected _sumFormula: FormulaArray;
    protected _rowFilter: FormulaArray;
    protected _columnFilter: FormulaArray;
    protected _emptyValue = '';

    constructor(data: any[][], sumFormula: any[][], rowFilter: any[][], columnFilter: any[][]){

        try { this._data = new RangeWithColumnNames(data); }
        catch(e){ throw new TypeError('"data" parameter has invalid value: ' + e.message); }

        try { this._sumFormula = new FormulaArray(sumFormula, this._data.columns); }
        catch(e){ throw new TypeError('"sumFormula" parameter has invalid value: ' + e.message); }

        try { this._rowFilter = new FormulaArray(rowFilter, this._data.columns, false); }
        catch(e){ throw new TypeError('"rowFilter" parameter has invalid value: ' + e.message); }

        try { this._columnFilter = new FormulaArray(columnFilter, this._data.columns); }
        catch(e){ throw new TypeError('"columnFilter" parameter has invalid value: ' + e.message); }

        if (this._sumFormula.length != this._columnFilter.length){
            throw new TypeError('"sumFormula" and "columnFilter" ranges must be same length');
        }
    }

    calc(): any[][]{
        let res = new Array(this._rowFilter.length).fill(null);
        res.forEach( (v, i) => {
            res[i] = new Array(this._columnFilter.length).fill(0.0);
        });

        for (let dataIndex = 0; dataIndex < this._data.length; dataIndex++){
            let rowParam = this._data.getRowArray(dataIndex + 1);
            for (let rowIndex = 0; rowIndex < this._rowFilter.length; rowIndex++){

                let rowIsEmpty = this._rowFilter.isEmpty(rowIndex);
                let rowFilterValue = rowIsEmpty || this._rowFilter.eval(rowIndex, rowParam);

                for (let columnIndex = 0; columnIndex < this._columnFilter.length; columnIndex++){

                    if (rowIsEmpty ||
                        this._columnFilter.isEmpty(columnIndex) ||
                        this._sumFormula.isEmpty(columnIndex)
                    ){
                        res[rowIndex][columnIndex] = this._emptyValue;
                        continue;
                    } 

                    if (!rowFilterValue || !this._columnFilter.eval(columnIndex, rowParam)) continue;

                    let sumValue = this._sumFormula.eval(columnIndex, rowParam);
                    res[rowIndex][columnIndex] += sumValue;
                }
            }
        }

        return res;
    }
}

export function pivot(data: any[][], sumFormula: any[][], rowFilter: any[][], columnFilter: any[][]): any[][] {
    let f = new PivotFunction(data, sumFormula, rowFilter, columnFilter);
    return f.calc();
}
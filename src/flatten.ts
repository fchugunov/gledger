import { RangeOnlyColumnNames } from './types'

class FlattenFunction {

    protected _data: any[][];
    protected _columns: string[];
    protected _rows: string[];

    constructor(columns: any[][], rows: any[][], data: any[][]){

        try { this._columns = new RangeOnlyColumnNames(columns).toArray(); }
        catch(e){ throw new TypeError('"columns" parameter has invalid value: ' + e.message); }

        try { this._rows = new RangeOnlyColumnNames(rows, false).toArray(); }
        catch(e){ throw new TypeError('"rows" parameter has invalid value: ' + e.message); }

        this._data = data;

        if (this._data.length != this._rows.length ||
            this._data.some(r => r.length != this._columns.length)
        ){
            throw new TypeError('"data" parameter has invalid value: rows and columns length do not match data size');
        }
    }

    calc(): any[][]{
        let res = new Array(this._rows.length * this._columns.length);

        for (let i = 0; i < this._columns.length; i++){
            for (let j = 0; j < this._rows.length; j++){
                let row = new Array(3);
                row[0] = this._columns[i];
                row[1] = this._rows[j];
                row[2] = this._data[j][i];
                res[i * this._rows.length + j] = row;
            }            
        }

        return res;
    }
}

export function flatten(columns: any[][], rows: any[][], data: any[][]): any[][] {
    let f = new FlattenFunction(columns, rows, data);
    return f.calc();
}
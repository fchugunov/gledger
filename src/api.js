/**
 * Creates accounting journal from documents list
 *
 * @param {'Docs'!B2:H} docs Documents list range (first row must have columns headers)
 * @param {'Types'!B2:H} types Document types list (first row must have columns headers)
 * @param {'Templates'!B2:H} entries Journal entries templates list (first row must have columns headers)
 * @param {'Types'!C2:D2} filter Filter columns list 
 * @param {'Templates'!C2:D2} groupby Group by columns list
 * @param {'Templates'!E2} sum Sum columns list

 * @return Accounting journal list
 * @customfunction
 */
function gl_journal(docs, types, entries, filter, groupby, sum){
    return gledger.groupby(gledger.join(gledger.filter(gledger.join(docs, types), filter), entries), groupby, sum); 
}

/**
 * Сonverts the pivot budget table into a flat list of entries
 *
 * @param {'Budget'!B2:H2} columns Dates row
 * @param {'Budget'!A2:A10} rows Budget items column
 * @param {'Budget'!B3:H10} data Range with costs values

 * @return List of entries
 * @customfunction
 */
function gl_flatten(columns, rows, data){
    return gledger.flatten(columns, rows, data); 
}

/**
 * Create general ledger account closing entries
 *
 * @param {'Journal'!B2:H} journal Accounting journal data range (first row must have columns headers) 
 * @param {'Closing templates'!B2:E} templates Closing journal templates range (first row must have columns headers) 
 * @param {'Closing templates'!B2} filter Filter columns list 
 * @param {'Closing templates'!C2:D2} groupby Group by columns list
 * @param {'Closing templates'!E2} sum Sum columns list

 * @return account closing entries
 * @customfunction
 */
function gl_close(journal, templates, filter, groupby, sum) 
{
    return gledger.filter(
        gledger.groupby(
            gledger.filter(
                gledger.join(journal, templates, true),
                filter
            ),
            groupby, sum
        ),
        "={" + sum + "} > 0"
    ); 
}

/**
 * Creates pivot report table from journal data
 *
 * @param {'Journal'!B2:H} data Accounting journal data range (first row must have columns headers) 
 * @param {I6:6} sumFormula Row with sum formulas
 * @param {H7:H} rowFilter Column with row filter formulas
 * @param {I5:5} columnFilter Row with column filter formulas

 * @return Report table
 * @customfunction
 */
function gl_pivot(data, sumFormula, rowFilter, columnFilter){
    return gledger.pivot(data, sumFormula, rowFilter, columnFilter); 
}


/**
 * Saves range to document cache and returns it from cache if range turns to empty
 *
 * @param {"documents"} key Range name
 * @param {'Docs'!B2:H} range Range data

 * @return Last non empty range
 * @customfunction
 */
function gl_cache(key, range) 
{
    return gledger.cache(key, range);
}

function gl_join(left, right, columns) 
{
	return gledger.join(left, right, columns);
}

function gl_groupby(range, groupby, sum) 
{
    return gledger.groupby(range, groupby, sum);
}

function gl_filter(range, filter) 
{
    return gledger.filter(range, filter);
}

import unzip from "pop-zip/unzip";

export class KeyMap {

    protected _data: Map<string, Array<number>>;

    constructor(){
        this._data = new Map<string, Array<number>>();
    }

    add(key: string, row: number){

        if (!this._data.has(key))
            this._data.set(key, new Array<number>());

        this._data.get(key).push(row);
    }

    get(key: string): number[]{
        return this._data.get(key);
    }
}

export class KeyMapArray {
    protected _data: Array<KeyMap>;

    constructor(levels: number){
        this._data = new Array<KeyMap>();
        for (let i = 0; i < levels; i++)
            this._data.push(new KeyMap());
    }

    static multyKeyFromArray(keyArray: string[]): string {
        return keyArray.join('__');
    }

    add(level: number, keys: string[], row: number){
        let keysString = KeyMapArray.multyKeyFromArray(keys);
        this._data[level].add(keysString, row);
    }

    get(keys: string[]): number[] {
        let res = null;

        for (let i = keys.length - 1; i >= 0 && res == null; i--){
            let keysString = KeyMapArray.multyKeyFromArray(keys.slice(0, i + 1));
            res = this._data[i].get(keysString);
        }

        return res;
    }
}

export class RangeOnlyColumnNames {

    protected _columns: string[];

    constructor(range: any[][] | string, isRow: boolean = true){
        let columnsRange: RangeWithColumnNames;
        let columnsTyped:any[][] = typeof range == 'string' ? [[range]] : range;
        if (!isRow){
            columnsTyped = unzip(columnsTyped);
        }
        columnsRange = new RangeWithColumnNames(columnsTyped);

        if (columnsRange.length != 0)
            throw new TypeError(`range must have only one ${isRow ? "row" : "column"}`);

        this._columns = columnsRange.columns;
    }

    toArray(): string[]{
        return this._columns;
    }
}

export class RangeWithColumnNames {

    protected _columnsMap: Map<string,  number>;
    public _data: Array<Array<any>>;

    constructor(range: any[][]){

        if (!RangeWithColumnNames.rangeHasFirstRowNonEmpty(range))
            throw new TypeError("Column names cannot be empty");

        this._data = range.filter(row => row.some(cell => cell != null && cell != ""));
        this._columnsMap = new Map();
        this._data[0].forEach((c, i) => {
            if (this._columnsMap.has(c))
                throw new TypeError(`Column with name "${c}" already exists`);

            this._columnsMap.set(c, i)
        });
    }

    static rangeHasFirstRowNonEmpty(range: any[][]): boolean{
        return range.length > 0 &&
            Array.isArray(range[0]) &&
            range[0].every(c => c != null && c != "");
    }

    addColumn(name: string, val: any){
        if (this._columnsMap.has(name))
            throw new TypeError(`Column with name "${name}" already exists`);

        this._data[0].push(name);
        this._columnsMap.set(name, this._data[0].length - 1);

        this.forEach(row => {
            row.push(val);
        });
    }

    getRow(row: number): Map<string, any>{

        let res = new Map<string, any>();

        this._columnsMap.forEach((i, c) => 
            res.set(c, this._data[row][i])
        )

        return res;
    }

    getRowObject(row: number){

        let res = this._data[row].reduce((o, v, i) => {
            o['x' + i] = v;
            return o;
        }, {});

        return res;
    }

    getRowArray(row: number): any[]{
        return this._data[row];
    }

    getByIndex(row: number, column: number){
        return this._data[row][column];
    }

    get(row: number, column: string){
        let columnIndex = this._columnsMap.get(column);
        return this._data[row][columnIndex];
    }

    columnIndex(column: string): number {
        return this._columnsMap.get(column);
    }

    get length(): number{
        return this._data.length - 1;
    }

    get columns(): string[]{
        return this._data[0];
    }

    forEach(fn, scope?) {
        for (let i = 1; i < this._data.length; i++) {
           fn.call(scope, this._data[i], i, this._data);
        }
    }
}

export class ColumnPair{
    public name: string;
    public index: number;

    constructor(name:string, index:number){
        this.name = name;
        this.index = index;
    }
}

import { RangeOnlyColumnNames, RangeWithColumnNames, ColumnPair, KeyMap, KeyMapArray } from './types'
import { FormulaExpression } from './formula'

class FilterFunction {

    protected _range: RangeWithColumnNames;
    protected _filterColumns: string[];
    protected _filterExpression: FormulaExpression;

    constructor(range: any[][], filter: any[][] | string){

        try { this._range = new RangeWithColumnNames(range); }
        catch(e){ throw new TypeError('"range" parameter has invalid value: ' + e.message); }    

        try { 
            if (typeof filter == 'string' && filter.startsWith('='))
                this._filterExpression = new FormulaExpression(filter, this._range.columns);
            else
                this._filterColumns = new RangeOnlyColumnNames(filter).toArray();
        }
        catch(e) {
            throw new TypeError('"filter" parameter has invalid value: ' + e.message);
        }

        let notFoundColumn = this._filterColumns != null && this._filterColumns.find(c => this._range.columns.indexOf(c) < 0);
        if (notFoundColumn) throw new TypeError(`range does not have "${notFoundColumn}" column specified in "filter" param`);
    }


    calc(): any[][]{
        let resArray = new Array();

        let headerRow = this._range.columns;
        resArray.push(headerRow);

        this._range.forEach((row, rowIndex) => {

            let take = this._filterColumns ? 
                this._filterColumns.every(colName => this._range.get(rowIndex, colName)) :
                this._filterExpression.eval(this._range.getRowArray(rowIndex));

            if (take) resArray.push(row);
        })

        return resArray;
    }
}

export function filter(range: any[][], filter: any[][] | string): any[][] {
    let f = new FilterFunction(range, filter);
    return f.calc();
}
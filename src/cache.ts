import { RangeOnlyColumnNames, RangeWithColumnNames, ColumnPair, KeyMap, KeyMapArray } from './types'

class CacheFunction {

    protected _range: any[][];
    protected _key: string;
    protected _cache: GoogleAppsScript.Cache.Cache;
    protected readonly _cacheMaxParts = 100;
    protected readonly _stringMaxLegth = 50 * 1000;
    protected readonly _expirationInSeconds = 21600;

    constructor(key: string, range: any[][]){
        this._key = key;
        this._range = Array.isArray(range) ? range : null;
        this._cache = CacheService.getDocumentCache();
    }

    rangeIsEmpty(): boolean{
        return (
            this._range == null ||
            this._range.length == 0
        );
    }

    getKey(part: number): string {
        return `${this._key}_${part}`;
    }

    getRangeFromCache(): any[][]{

        let res: any[][] = null;

        let cacheString = "";

        for (let i = 0; i < this._cacheMaxParts; i++){
            let key = this.getKey(i);
            let val = this._cache.get(key);

            if (val == null) break;

            cacheString += val;
        }

        if (cacheString){
            res = JSON.parse(cacheString);
        }

        return res;
    }

    saveRangeToCache() {
        let cacheString = JSON.stringify(this._range);

        for (let i = 0; i * this._stringMaxLegth < cacheString.length; i++){

            if (i > this._cacheMaxParts) throw new TypeError("Range size is too big");
            
            let key = this.getKey(i);
            let val = cacheString.slice(i * this._stringMaxLegth, (i + 1) * this._stringMaxLegth);
            this._cache.put(key, val, this._expirationInSeconds);
        }
    }

    calc(): any[][]{
        if (this.rangeIsEmpty()){            
            return this.getRangeFromCache();
        }
        this.saveRangeToCache();
        return this._range;
    }
}

export function cache(key: string, range: any[][]): any[][] {
    let f = new CacheFunction(key, range);
    return f.calc();
}
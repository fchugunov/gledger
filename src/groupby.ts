import { RangeOnlyColumnNames, RangeWithColumnNames, ColumnPair, KeyMap, KeyMapArray } from './types'

class GroupByFunction {

    protected _range: RangeWithColumnNames;
    protected _groupbyColumns: string[];
    protected _sumColumns: string[];

    constructor(range: any[][], groupby: any[][] | string, sum: any[][] | string){

        try { this._range = new RangeWithColumnNames(range); }
        catch(e){ throw new TypeError('"range" parameter has invalid value: ' + e.message); }    

        try { this._groupbyColumns = new RangeOnlyColumnNames(groupby).toArray(); }
        catch(e){ throw new TypeError('"groupby" parameter has invalid value: ' + e.message); }

        try { this._sumColumns = new RangeOnlyColumnNames(sum).toArray(); }
        catch(e){ throw new TypeError('"sum" parameter has invalid value: ' + e.message); }

        let notFoundColumn = this._groupbyColumns.find(c => this._range.columns.indexOf(c) < 0);
        if (notFoundColumn) throw new TypeError(`range does not have "${notFoundColumn}" column specified in "groupby" param`);

        notFoundColumn = this._sumColumns.find(c => this._range.columns.indexOf(c) < 0);
        if (notFoundColumn) throw new TypeError(`range does not have "${notFoundColumn}" column specified in "sum" param`);
    }

    calc(): any[][]{
        let resArray = new Array();
        let resMap = new Map();

        let headerRow = this._groupbyColumns.concat(this._sumColumns);
        resArray.push(headerRow);

        this._range.forEach((row, rowIndex) => {

            let sumMap = resMap;
            let keysArray = [];
            let resRowIndex;

            this._groupbyColumns.forEach((colName, colIndex) => {

                let key = this._range.get(rowIndex, colName);
                keysArray.push(key);

                if (colIndex == this._groupbyColumns.length - 1){
                    resRowIndex = sumMap.get(key);
                    if (resRowIndex == null) {
                        let newRow = keysArray.concat(new Array(this._sumColumns.length).fill(0));
                        resRowIndex = resArray.push(newRow) - 1;
                        sumMap.set(key, resRowIndex);
                    }
                } else {
                    let nextMap = sumMap.get(key);

                    if (nextMap == null) {
                        nextMap = new Map();
                        sumMap.set(key, nextMap);
                    }

                    sumMap = nextMap;
                }
            })

            this._sumColumns.forEach((colName, colIndex) => {
                let resColIndex = colIndex + this._groupbyColumns.length;

                let sumVal = this._range.get(rowIndex, colName);

                if (typeof sumVal == 'string'){
                    sumVal = parseFloat(sumVal.replace(',', '.'))
                }

                resArray[resRowIndex][resColIndex] += sumVal;
            })
        })

        return resArray;
    }
}

export function groupby(range: any[][], groupby: any[][] | string, sum: any[][] | string): any[][] {
    let f = new GroupByFunction(range, groupby, sum);
    return f.calc();
}
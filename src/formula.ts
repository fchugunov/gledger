import { Parser, Expression } from 'expr-eval'
import format from 'tiny-date-format';

const FormulaParser = new Parser({
    operators: {
        'in': true
    }
});

FormulaParser.functions.date = s => {
    let pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
    return new Date(s.replace(pattern,'$3-$2-$1'));
}

FormulaParser.functions.ind = (x, i) => {
    return x[i];
}

FormulaParser.functions.format = (d, f) => {
    return format(d, f);
}

FormulaParser.functions.startsWith = (str, word) => {
    return str.lastIndexOf(word, 0) === 0;
}

export class FormulaExpression {

    protected _expression: Expression;

    constructor(formula: string, columns: string[]) {

        if (typeof formula != 'string' || !formula.startsWith('=')){
            throw new TypeError(`Formula parse error: formula must be string starting with "="`);
        }

        let formulaText = formula.substr(1);

        formulaText = formulaText.replace(new RegExp('[0-9],[0-9]', 'g'), m => m.replace(',', '.'))

        formulaText = formulaText.replace(new RegExp('\{.*?\}', 'g'), m => {
            let colName = m.slice(1, m.length - 1);
            let colIndex = columns.indexOf(colName);

            if (colIndex == -1)
                throw new TypeError(`Formula '${formula}' parse error: data range does not have column "${colName}"`);

            return 'ind(x,' + colIndex + ')';
        } );

        try{
            this._expression = FormulaParser.parse(formulaText);
        }
        catch(e){
            throw new TypeError(`Formula '${formula}' parse error: ${e.message}`)
        }
    }

    eval(data: any[]){
        return this._expression.evaluate({'x': data as any});
    }
}
